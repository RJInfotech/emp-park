﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emp.Carpark.Exceptions;

namespace Emp.Carpark
{
    class Program
    {
        static void Main(string[] args)
        {
            GetInput();
        }

        static void GetInput()
        {
            Console.Clear();
            Console.WriteLine("/****************************************************/");
            Console.WriteLine("/                Parking Fee Calculator              /");
            Console.WriteLine("/****************************************************/");
            Console.Write("Enter car park entry date and time(yyyy-MM-dd h:mi:ss) :");
            string entry = Console.ReadLine();
            Console.Write("Enter car park exit date and time(yyyy-MM-dd h:mi:ss) :");
            string exit = Console.ReadLine();
            DateTime entryDateTime, exitDateTime;
            if (DateTime.TryParse(entry, out entryDateTime) && DateTime.TryParse(exit, out exitDateTime))
            {
                try
                {
                    var result = CarParkCalculator.CalculateParkingFee(entryDateTime, exitDateTime);
                    Console.WriteLine($"Name: {result.Name}");
                    Console.WriteLine($"Total Fee: {result.Value}");
                }
                catch (InvalidParkingException e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            else
            {
                Console.WriteLine("Invalid date/time");
            }

            Console.WriteLine("Do you try with another combination to continue (y/n)?");
            var preference = Console.ReadLine();
            if (preference != null && preference.Equals("y", StringComparison.OrdinalIgnoreCase))
            {
                GetInput();
            }
        }
    }
}
