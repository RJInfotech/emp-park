﻿namespace Emp.Carpark.Extensions
{
    public static class DoubleExtensions
    {
        public static bool IsBetween(this double val, double start, double end, bool startInclusive, bool endInclusive)
        {
            bool startCondition = startInclusive ? val >= start : val > start;
            bool endCondition = endInclusive ? val <= end : val < end; 
            return startCondition && endCondition;
        }
    }
}