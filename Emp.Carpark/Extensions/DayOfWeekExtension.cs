﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Emp.Carpark.Extensions
{
    public static class DayOfWeekExtension
    {
        public static bool IsWeekend(this DayOfWeek dow)
        {
            return dow == DayOfWeek.Saturday || dow == DayOfWeek.Sunday;
        }
    }
}
