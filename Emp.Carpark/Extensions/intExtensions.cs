﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Emp.Carpark.Extensions
{
    public static class IntExtensions
    {
        public static bool IsBetween(this int val, int start, int end)
        {
            return val >= start && val <= end;
        }
    }
}
