﻿using System;
using Emp.Carpark.Exceptions;
using Emp.Carpark.Extensions;

namespace Emp.Carpark.Strategies
{
    public class WeekendParkingStrategy : IFlatRateParkingStrategy
    {
        public bool IsParkingConditionValid(DateTime entryDateTime, DateTime exitDateTime)
        {
            if (exitDateTime < entryDateTime)
            {
                throw new InvalidParkingException();
            }

            if (entryDateTime.DayOfWeek.IsWeekend() 
                && exitDateTime.DayOfWeek.IsWeekend() 
                && (exitDateTime - entryDateTime).TotalHours < 48)
            {
                return true;
            }

            return false;
        }

        public double FlatRate => 10;
        public string Name => "Weekend Rate";
    }
}