﻿using System;

namespace Emp.Carpark.Strategies
{
    interface IFlatRateParkingStrategy : IParkingStrategy
    {
        double FlatRate { get; }

        bool IsParkingConditionValid(DateTime entryDateTime, DateTime exitDateTime);
    }
}