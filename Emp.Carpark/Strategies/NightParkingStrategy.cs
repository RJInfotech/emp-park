﻿using System;
using Emp.Carpark.Exceptions;

namespace Emp.Carpark.Strategies
{
    public class NightParkingStrategy: IFlatRateParkingStrategy
    {
        public bool IsParkingConditionValid(DateTime entryDateTime, DateTime exitDateTime)
        {
            if (exitDateTime < entryDateTime)
            {
                throw new InvalidParkingException();
            }

            if (entryDateTime.Hour >= 18 && entryDateTime.Hour <= 23
                && exitDateTime <= entryDateTime.Date.AddDays(1).AddHours(6))
            {
                return true;
            }

            return false;
        }

        public double FlatRate => 6.5;
        public string Name => "Night Rate";
    }
}