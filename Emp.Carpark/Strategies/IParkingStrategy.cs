﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NodaTime;

namespace Emp.Carpark.Strategies
{
    interface IParkingStrategy
    {
        string Name { get; }
    }
}
