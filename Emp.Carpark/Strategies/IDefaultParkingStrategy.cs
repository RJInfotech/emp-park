using System;

namespace Emp.Carpark.Strategies
{
    interface IDefaultParkingStrategy : IParkingStrategy
    {
        double GetParkingRate(DateTime entryDateTime, DateTime exitDateTime);
    }
}