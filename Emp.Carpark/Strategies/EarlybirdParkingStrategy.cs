﻿using System;
using Emp.Carpark.Exceptions;
using Emp.Carpark.Extensions;

namespace Emp.Carpark.Strategies
{
    public class EarlybirdParkingStrategy : IFlatRateParkingStrategy
    {
        public double FlatRate => 13;
        public bool IsParkingConditionValid(DateTime entryDateTime, DateTime exitDateTime)
        {
            if (exitDateTime < entryDateTime)
            {
                throw new InvalidParkingException();
            }

            if (entryDateTime.Date == exitDateTime.Date
                && entryDateTime.TimeOfDay.TotalMinutes.IsBetween(360, 540, true, true)
                && exitDateTime.TimeOfDay.TotalMinutes.IsBetween(930, 1410, true, true))
            {
                return true;
            }

            return false;
        }

        public string Name => "Early Bird";
    }
}