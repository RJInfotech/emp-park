﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emp.Carpark.Exceptions;
using NodaTime;

namespace Emp.Carpark.Strategies
{
    public class DefaultParkingStrategy: IDefaultParkingStrategy
    {
        public static readonly Dictionary<double, double> Rates = new Dictionary<double, double>()
        {
            { 1, 5 },
            { 2, 10 },
            { 3, 15 }
        };

        public static readonly double MaxDayRate = 20;

        public double GetParkingRate(DateTime entryDateTime, DateTime exitDateTime)
        {
            if (exitDateTime < entryDateTime)
            {
                throw new InvalidParkingException();
            }
            double totalRate;

            TimeSpan totalSpan = exitDateTime - entryDateTime;

            if (entryDateTime.Date != exitDateTime.Date)
            {
                double totalDays = totalSpan.TotalDays + 1;
                double billableDays = Math.Ceiling(totalDays);
                totalRate = billableDays * MaxDayRate;
            }
            else
            {
                double totalHours = totalSpan.TotalHours;
                double billableHours = Math.Ceiling(totalHours);
                if (!Rates.TryGetValue(billableHours, out totalRate))
                {
                    totalRate = MaxDayRate;
                }
            }


            return totalRate;
        }

        public string Name => "Standard Rate";
    }
}
