﻿namespace Emp.Carpark
{
    public class ParkingFee
    {
        public string Name { get; set; }
        public double Value { get; set; }
    }
}