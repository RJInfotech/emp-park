﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Emp.Carpark.Exceptions
{
    public class InvalidParkingException : Exception
    {
        public InvalidParkingException()
            : base("Invalid parking.  Check entry/exit conditions")
        {
            
        }
    }
}
