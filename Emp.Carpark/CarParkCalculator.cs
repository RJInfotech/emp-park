﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emp.Carpark.Exceptions;
using Emp.Carpark.Strategies;

namespace Emp.Carpark
{
    public static class CarParkCalculator
    {
        public static ParkingFee CalculateParkingFee(DateTime entry, DateTime exit)
        {
            if (exit < entry)
            {
                throw new InvalidParkingException();
            }

            IFlatRateParkingStrategy[] flatRateParkingStrategies = new[]
            {
                (IFlatRateParkingStrategy)new EarlybirdParkingStrategy(),
                (IFlatRateParkingStrategy)new NightParkingStrategy(),
                (IFlatRateParkingStrategy)new WeekendParkingStrategy()
            };

            ParkingFee parkingFee = null;
            // Choose the cheapest flate rate
            foreach (var flatRateParkingStrategy in flatRateParkingStrategies)
            {
                if (flatRateParkingStrategy.IsParkingConditionValid(entry, exit))
                {
                    if (parkingFee == null ||
                        parkingFee.Value > flatRateParkingStrategy.FlatRate)
                    {
                        parkingFee = new ParkingFee()
                        {
                            Name = flatRateParkingStrategy.Name,
                            Value = flatRateParkingStrategy.FlatRate
                        };
                    }
                }
            }

            if (parkingFee == null)
            {
                DefaultParkingStrategy defaultParkingStrategy = new DefaultParkingStrategy();
                parkingFee = new ParkingFee()
                {
                    Name = defaultParkingStrategy.Name,
                    Value = defaultParkingStrategy.GetParkingRate(entry, exit)
                };
            }

            return parkingFee;
        }
    }
}
