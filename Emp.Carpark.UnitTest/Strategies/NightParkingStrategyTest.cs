﻿using System;
using Emp.Carpark.Exceptions;
using Emp.Carpark.Strategies;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Emp.Carpark.UnitTest.Strategies
{
    [TestClass]
    public class NightParkingStrategyTest
    {
        private NightParkingStrategy _subject;

        private TestContext testContextInstance;
        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }


        [TestInitialize]
        public void Initialize()
        {
            _subject = new NightParkingStrategy();
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidParkingException))]
        public void IsParkingConditionValid_InvalidEntryExitCondition_ThrowsException()
        {
            _subject.IsParkingConditionValid(DateTime.Now, DateTime.Now.AddDays(-1));
        }

        [TestMethod]
        [DeploymentItem("NightParkingStrategy.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
            "NightParkingStrategy.xml",
            "Row",
            DataAccessMethod.Sequential)]
        public void IsParkingConditionValid_ShouldReturnExpectedValidation()
        {
            bool isValid = _subject.IsParkingConditionValid(DateTime.Parse(TestContext.DataRow["Entry"].ToString()),
                DateTime.Parse(TestContext.DataRow["Exit"].ToString()));
            Assert.AreEqual(Convert.ToBoolean(TestContext.DataRow["Result"]), isValid,
                $"Entry: {TestContext.DataRow["Entry"]}, Exit: {TestContext.DataRow["Exit"]} should be {TestContext.DataRow["Result"]} condition");
        }
    }
}