﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emp.Carpark.Exceptions;
using Emp.Carpark.Strategies;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Emp.Carpark.UnitTest.Strategies
{
    [TestClass]
    public class DefaultParkingStrategyTest
    {
        private DefaultParkingStrategy _subject;

        private TestContext testContextInstance;
        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }


        [TestInitialize]
        public void Initialize()
        {
            _subject = new DefaultParkingStrategy();
        }

        [TestMethod]
        [DeploymentItem("DefaultParkingStrategy.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
                   "DefaultParkingStrategy.xml",
                   "Row",
                    DataAccessMethod.Sequential)]
        public void DefaultParkingStrategy_ShouldCalculateBillableRate()
        {
            double totalRate = _subject.GetParkingRate(DateTime.Parse(TestContext.DataRow["Entry"].ToString()),
                DateTime.Parse(TestContext.DataRow["Exit"].ToString()));
            Assert.AreEqual(Convert.ToDouble(TestContext.DataRow["Result"]), totalRate,
                $"Entry: {TestContext.DataRow["Entry"]}, Exit: {TestContext.DataRow["Exit"]} should charge {TestContext.DataRow["Result"]}");
        }
    }
}
