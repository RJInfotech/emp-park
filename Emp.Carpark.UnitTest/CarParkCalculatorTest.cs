﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emp.Carpark.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Emp.Carpark.UnitTest
{
    [TestClass]
    public class CarParkCalculatorTest
    {
        private TestContext testContextInstance;
        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidParkingException))]
        public void CalculateParkingFee_InvalidEntryExitCondition_ThrowsException()
        {
            CarParkCalculator.CalculateParkingFee(DateTime.Now, DateTime.Now.AddDays(-1));
        }

        [TestMethod]
        [DeploymentItem("CarParkCalculator.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
            "CarParkCalculator.xml",
            "Row",
            DataAccessMethod.Sequential)]
        public void CalculateParkingFee_ShouldCalculateBillableRate()
        {
            ParkingFee pf = CarParkCalculator.CalculateParkingFee(DateTime.Parse(TestContext.DataRow["Entry"].ToString()),
                DateTime.Parse(TestContext.DataRow["Exit"].ToString()));
            Assert.AreEqual(TestContext.DataRow["Name"].ToString(), pf.Name,
                $"Entry: {TestContext.DataRow["Entry"]}, Exit: {TestContext.DataRow["Exit"]} should be {TestContext.DataRow["Name"]} condition");
            Assert.AreEqual(Convert.ToDouble(TestContext.DataRow["Fee"]), pf.Value,
                $"Entry: {TestContext.DataRow["Entry"]}, Exit: {TestContext.DataRow["Exit"]} should be {TestContext.DataRow["Fee"]} condition");
        }
    }
}
